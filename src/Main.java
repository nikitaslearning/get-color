import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class Main {

    public static void main(String[] args) {

        BufferedImage bufferedImage = null;

        try {
            bufferedImage = ImageIO.read(new File("C:\\Users\\308324\\Desktop\\hoc-2018-creativity.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        //       create pool for picsels
        HashMap<Integer, Integer> colorsPool = new HashMap<>();
        int imgX = (bufferedImage.getWidth() / 2) - 10;
        int imgY = (bufferedImage.getHeight() / 2) - 10;


        System.out.print(bufferedImage.getWidth());
        System.out.print("x");
        System.out.println(bufferedImage.getHeight());

        //       put one picsel to the pool
        int color = bufferedImage.getRGB(imgX - 1, imgY);
        colorsPool.put(color, 1);

        for (int y = imgY; y < imgY + 20; y++) {
            for (int x = imgX; x < imgX + 20; x++) {
                color = bufferedImage.getRGB(x, y);

                if (colorsPool.containsKey(color)) {
                    colorsPool.put(color, colorsPool.get(color) + 1);
                } else {
                    colorsPool.put(color, 1);
                }
            }
        }

        System.out.println("-----------------------------");
        for (Integer key : colorsPool.keySet()) {
            System.out.println();
            int val = colorsPool.get(key);
            System.out.println(key.toString() + "  " + val);

            if (colorsPool.get(color) < val){
                color = key;
            }
        }
        System.out.println("===============================");

        int red = (color & 0xff0000) >> 16;
        int green = (color & 0xff00) >> 8;
        int blue = color & 0xff;

        System.out.println("*" + color + "*");
        System.out.println();
        System.out.println(red);
        System.out.println(green);
        System.out.println(blue);
    }

}
